import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';
import { TiendaPageRoutingModule } from './tienda/tienda-routing.module';
import { IonicModule } from '@ionic/angular';
import { CommonModule } from '@angular/common';

const routes: Routes = [
 
  // {
  //   path: '',
  //   loadChildren: () => import('./home/home.module').then( m => m.HomePageModule)
  // },
  {
    path: '',
    redirectTo: 'tienda',
    pathMatch: 'full'
  },

  {
  //   /tienda
  //    /tienda/TiendaId
  //     /tienda/borrar
    path: 'tienda',
    children:[
      {
        path:'',
        loadChildren: () => import('./tienda/tienda.module').then( m => m.TiendaPageModule)
      },
      {
        path: ':flat/:id',
        loadChildren: () => import ('./tienda/detalle/detalle.module').then(m => m.DetallePageModule)
      },
      {
        path: ':flat/:id/:cat/:catId',
        loadChildren: () => import ('./tienda/detalle/item/item.module').then(m => m.ItemPageModule)
      },
      {
        path: ':tiendaId/add',
        loadChildren: () => import ('./tienda/detalle/add/add.module').then(m => m.AddPageModule)
      }
    ]
    },
    {
    path: 'map',
    children:[
      {
        path:'',
        loadChildren: () => import('./shared/shared.module').then(m => m.SharedModule)
      }
    ]
    
    // {
    //   path: 'tiendaId',
    //   children:[
    //   {
    //     path:'',
    //     loadChildren: () => import ('./tienda/detalle/detalle.module').then(m => m.DetallePageModule)
    //   },
    //   {
    //     path: ':itemId',
    //     loadChildren: () => import ('./tienda/detalle/item/item.module').then(m => m.ItemPageModule)
    //   }
     
  

  }]

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules }),
    IonicModule,
    CommonModule
  ],
  exports: [RouterModule],
  
})
export class AppRoutingModule { }
