import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { RouteReuseStrategy } from '@angular/router';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule } from '@angular/forms';
import { IonicModule, IonicRouteStrategy } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';
import{ AngularFireModule} from 'angularfire2';
import {AngularFirestoreModule} from 'angularfire2/firestore';
import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';
import { LocationPickerComponent } from './shared/pickers/location-picker/location-picker.component';
import { MapModalComponent } from './shared/map-modal/map-modal.component';

export const firebase ={
 
    apiKey: 'AIzaSyD0cDXncqbH4ecK1TijCTzE_ftPeDCb07Q',
    authDomain: 'proyecto-3270a.firebaseapp.com',
    databaseURL: 'https://proyecto-3270a.firebaseio.com',
    projectId: 'proyecto-3270a',
    storageBucket: 'proyecto-3270a.appspot.com',
    messagingSenderId: '559637611116',
    appId: '1:559637611116:web:c61fe987fdcb9cf4cece49',
    measurementId: 'G-4W2Q4T7FNM'
}

@NgModule({
  declarations: [AppComponent],
  entryComponents: [],
  imports: [
    // BrowserModule, 
    // HttpClientModule, 
    // IonicModule.forRoot(), 
    // AppRoutingModule,
    // MapModalComponent,
    // LocationPickerComponent,
    // FormsModule,
    // AngularFireModule,
    // AngularFirestoreModule,
    // AngularFireModule.initializeApp(firebase)
    BrowserModule,
    HttpClientModule,
    IonicModule.forRoot(),
    AppRoutingModule,
    AngularFireModule,
    AngularFirestoreModule,
    AngularFireModule.initializeApp(firebase)
  ],

  providers: [
    StatusBar,
    SplashScreen,
    { provide: RouteReuseStrategy, useClass: IonicRouteStrategy }
  ],
  bootstrap: [AppComponent]
})
export class AppModule {}


