import { Component, OnInit } from '@angular/core';
import { tienda } from '../tienda/tienda.model'
import { detailed } from '../tienda/tienda.model'
import { TiendaService } from '../tienda.service';
import { ActivatedRoute, Router } from '@angular/router';
import { AlertController } from '@ionic/angular';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage implements OnInit {

  destacados: detailed [];

  constructor( 
    private route: ActivatedRoute,
    private servicio: TiendaService,
    private router: Router,
    private alert: AlertController) { }

  ngOnInit(){}


  ionViewWillEnter(){
    this.destacados = this.servicio.getDestacados();
    //console.log(this.destacados);
    console.log('will enter again');
  }


}
