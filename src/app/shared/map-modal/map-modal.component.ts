import { Component, OnInit, ViewChild, ElementRef, Renderer2 } from '@angular/core';
import { ModalController } from '@ionic/angular'
import { map } from 'rxjs/operators';
import { environment } from '../../../environments/environment'

@Component({
  selector: 'app-map-modal',
  templateUrl: './map-modal.component.html',
  styleUrls: ['./map-modal.component.scss'],
})
export class MapModalComponent implements OnInit {
  @ViewChild('map') mapElement: ElementRef;
  private apiKey = environment.googleMapApi;
  constructor(private modal: ModalController, 
    private render: Renderer2
    ) { }

  ngOnInit() {}
  ngAfterViewInit() {
    //functions execute after html loads
    this.getGoogleMaps().then( google =>{
      const mapEl = this.mapElement.nativeElement;
      const map = new google.Map(mapEl, {
        center: {lat: -34.397, lng: 150.644},
        zoom: 16
      });
      google.event.addListenerOnce(map, 'idle', () => {
        this.render.addClass(mapEl, 'visible');
      })
      map.addListener('click', event => {
        const selected = {lat: event.latLong.lat() , lng: event.latLong.long() };
      })
    }).catch(err => {
      console.log(err)
    });
    
  }
  onCancel(){
    this.modal.dismiss
  }

  private getGoogleMaps(): Promise<any>{
    // map has already loaded: return, else load it
    const win = window as any;
    const googleModule = win.google;

    if(googleModule && googleModule.maps){
      return Promise.resolve(googleModule.maps)
    }
    return new Promise((resolve, reject) => {
      const script = document.createElement('script');
      script.src = 'https://maps.googleapis.com/api/js?key=' + this.apiKey;
      script.async = true;
      script.defer = true;
      document.body.appendChild(script);
      script.onload = () => {
        const loadedGoogleModule = win.google;
        if(loadedGoogleModule && loadedGoogleModule.maps) {
          resolve(loadedGoogleModule.maps);
        } else {
          reject('Google maps SDK no disponible')
        }
      }
    })
  }
}
