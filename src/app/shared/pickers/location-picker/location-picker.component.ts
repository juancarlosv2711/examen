import { Component, OnInit, EventEmitter, Output } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { MapModalComponent } from '../../map-modal/map-modal.component';
import { CommonModule } from '@angular/common';
import { HttpClientModule, HttpClient } from '@angular/common/http';
import { environment } from '../../../../environments/environment';
import { placeLocation } from './location-picker.model';
import { map, switchMap } from 'rxjs/operators';
import { of } from 'rxjs';
@Component({
  selector: 'app-location-picker',
  templateUrl: './location-picker.component.html',
  styleUrls: ['./location-picker.component.scss'],
})

export class LocationPickerComponent implements OnInit {

  selectedLocationImage: string;
  @Output() locationPick = new EventEmitter <placeLocation>();
  constructor(
    private modal:ModalController,
    private http: HttpClient) { }


  onPickLocation(){
    this.modal.create({
      component: MapModalComponent
    }).then(
        modalEl => {
          modalEl.onDidDismiss().then(
            selected => {
              if (!selected.data) {
                return;
              }
              const pickerlocation: placeLocation = {
                lat: selected.data.lat,
                lng: selected.data.lng,
                address: null,
                staticMapImageUrl: null
              };
              this.getAddress(selected.data.lat, selected.data.lng).pipe(
                switchMap(address => {
                  pickerlocation.address = address;
                  return of (this.getMapImage(selected.data.lat, selected.data.lng, 14))
                })
              ).subscribe(
                staticMapImage => {
                  pickerlocation.staticMapImageUrl= staticMapImage,
                  this.selectedLocationImage = staticMapImage,
                  this.locationPick.emit(pickerlocation);
                }
              );
            }
          );
        modalEl.present();
      }
    )
  }
  ngOnInit() {}
   
  private getAddress(lat: number, lng: number){
    return this.http.get <any>(
      `https://maps.googleapis.com/maps/api/geocode/json?latlng=${lat},${lng}&key=${environment.googleMapApi}`
    ).pipe(map(geodata => {
      if (!geodata){
        //No data return null
        return null;
      }
      //Return geoodata
      return geodata.results[0].formatted_address;
    }));
  }
  private getMapImage(lat: number, lng: number, zoom: number){
    return <string>
    `https://maps.googleapis.com/maps/api/staticmap?center=${lat},${lng}&zoom=${zoom}&size=500x300&maptype=roadmap
    &markers=color:red%7Clabel:C%7C${lat},${lng}
    &key=${environment.googleMapApi}`;
  }
}

