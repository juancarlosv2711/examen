import { Injectable, ɵisBoundToModule__POST_R3__, DefaultIterableDiffer } from '@angular/core';
import { tienda } from './tienda/tienda.model';
import { detailed } from './tienda/tienda.model';
import { isNull } from 'util';
import { HttpClientModule, HttpClient } from '@angular/common/http';
import { AngularFirestore, AngularFirestoreCollection, AngularFirestoreDocument } from 'angularfire2/firestore';
import { Observable } from 'rxjs';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import { map } from 'rxjs/operators';
import { filter, switchMap } from 'rxjs/operators';
import 'rxjs/add/operator/map'

 
@Injectable({
  providedIn: 'root'
})
export class TiendaService {

  /////////////// TEST OLD SPORT ---------------------------------------------
  item: detailed[]; 
  /////////////// TEST OLD SPORT ---------------------------------------------
  private tiendaVar: Observable<tienda[]>;
  private catVar: Observable<detailed[]>;
  private tiendaRef: AngularFirestoreCollection<tienda>;
  private catRef: AngularFirestoreCollection<detailed>;
 // private docRef: AngularFirestoreDocument<tienda>;
  // private tiendas: tienda[] = [
  //   {
  //     id: 1,
  //     title: "Apartamentos",
  //     description: "Pisos acogedores, comodos, amoblados",
  //     image: 'https://images.unsplash.com/photo-1519801671298-75871a4d9bfc?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=808&q=80',
  //     modelos: [
  //       {
  //         id: 1,
  //         name: 'Moderno',
  //         category: '1 Hab',
  //         image: 'https://images.unsplash.com/photo-1484154218962-a197022b5858?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=1353&q=80',
  //         price: 20
  //       },
  //       {
  //         id: 2,
  //         name: 'Lujoso',
  //         category: '2 Hab',
  //         image: 'https://images.unsplash.com/photo-1512918728675-ed5a9ecdebfd?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=1350&q=80',
  //         price: 22
  //       },
  //       {
  //         id: 3,
  //         name: 'Moderno',
  //         category: '1 Hab',
  //         image: 'https://images.unsplash.com/photo-1486304873000-235643847519?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=1489&q=80',
  //         price: 22
  //       }
  //     ]
  //   },
  //   {
  //     id: 2,
  //     title: "Casas",
  //     description: "Casas familiares",
  //     image: 'https://images.unsplash.com/photo-1524082983062-21c24967d6c9?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=858&q=80',
  //     modelos: [
  //       {
  //         id: 1,
  //         name: 'Moderno',
  //         category: '3 Hab',
  //         image: 'https://images.unsplash.com/photo-1512917774080-9991f1c4c750?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=1350&q=80',
  //         price: 35
  //       },
  //       {
  //         id: 2,
  //         name: 'Escandinavo',
  //         category: '2 Hab',
  //         image: 'https://images.unsplash.com/photo-1554995207-c18c203602cb?ixlib=rb-1.2.1&auto=format&fit=crop&w=1350&q=80',
  //         price: 30
  //       },
  //       {
  //         id: 3,
  //         name: 'Industrial',
  //         category: '4 Hab',
  //         image: 'https://images.unsplash.com/photo-1512916194211-3f2b7f5f7de3?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=1050&q=80',
  //         price: 35
  //       },
  //       {
  //         id: 4,
  //         name: 'Rustico',
  //         category: '3 Hab',
  //         image: 'https://images.unsplash.com/photo-1511968168651-d7d4e6daba8d?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=1000&q=80',
  //         price: 40
  //       }
  //     ]
  //   },
  //   {
  //     id: 3,
  //     title: "Cabañas",
  //     description: "Chaqueta denim",
  //     image: 'https://images.unsplash.com/photo-1533230464445-e01ef07c65c5?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=668&q=80',
  //     modelos: [
  //       {
  //         id: 1,
  //         name: 'Jardin',
  //         category: '1 Hab',
  //         image: 'https://images.unsplash.com/photo-1588880331179-bc9b93a8cb5e?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=1350&q=80',
  //         price: 50
  //       },
  //       {
  //         id: 2,
  //         name: 'TreeHouse',
  //         category: '2 Hab',
  //         image: 'https://images.unsplash.com/photo-1550355191-aa8a80b41353?ixlib=rb-1.2.1&auto=format&fit=crop&w=668&q=80',
  //         price: 55
  //       }
  //     ]
  //   },
  //   {
  //     id: 4,
  //     title: "Mansiones",
  //     description: "Zapatos de distintos colores y modelos",
  //     image: 'https://images.unsplash.com/photo-1462927114214-6956d2fddd4e?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=1349&q=80',
  //     modelos: [
  //       {
  //         id: 1,
  //         name: 'Moderno',
  //         category: '12 Hab',
  //         image: 'https://images.unsplash.com/photo-1503174971373-b1f69850bded?ixlib=rb-1.2.1&auto=format&fit=crop&w=2587&q=80',
  //         price: 140
  //       },
  //       {
  //         id: 2,
  //         name: 'Rustico',
  //         category: '10 Hab',
  //         image: 'https://images.unsplash.com/photo-1523688471150-efdd09f0f312?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=1000&q=80',
  //         price: 155
  //       },
  //       {
  //         id: 3,
  //         name: 'Clasico',
  //         category: '8 Hab',
  //         image: 'https://images.unsplash.com/photo-1506126279646-a697353d3166?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=1350&q=80',
  //         price: 193
  //       },
  //       {
  //         id: 4,
  //         name: 'Palacio',
  //         category: '32 Hab',
  //         image: 'https://images.unsplash.com/photo-1556821862-33ec0be5c2c0?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=1351&q=80',
  //         price: 205
  //       },
  //       {
  //         id: 5,
  //         name: 'Clasico',
  //         category: '15 Hab',
  //         image: 'https://images.unsplash.com/photo-1562932831-afcfe48b5786?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=1389&q=80',
  //         price: 210
  //       },
  //       {
  //         id: 6,
  //         name: 'Rustico',
  //         category: '16 Hab',
  //         image: 'https://images.unsplash.com/photo-1416331108676-a22ccb276e35?ixlib=rb-1.2.1&auto=format&fit=crop&w=1347&q=80',
  //         price: 230
  //       },
  //       {
  //         id: 7,
  //         name: 'Oriental',
  //         category: '29 Hab',
  //         image: 'https://images.unsplash.com/photo-1550499190-1b823e9650ed?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=668&q=80',
  //         price: 225
  //       }
  //     ]
  //   }
  // ];
  constructor(
    private db: AngularFirestore
  ) { }
  // getAllPhones() {
  //   return [...this.tiendas]
  // }
  getAllPhones() {

    // this.tiendaRef = this.db.collection('flat');
    // console.log(this.tiendaRef.snapshotChanges());
    // console.log('h');
    // this.tiendaVar = this.tiendaRef.valueChanges();
    // console.log(this.tiendaVar);
    // return this.tiendaVar;


   this.tiendaRef = this.db.collection('flat');
    this.tiendaRef.snapshotChanges()
    console.log('h');
    this.tiendaVar = this.tiendaRef.valueChanges({ idField: 'id' });
    //this.tiendaVar.subscribe(res => console.log(res));
    return this.tiendaVar;
 
  }

  getDestacados() {

    // for (var _i = 0; _i < this.tiendas.length; _i++) {
    //   for (var _j = 0; _j < this.tiendas[_i].modelos.length; _i++) {
    //     switch (_i) {
    //       case 1:
    //         this.item.push(this.tiendas[_i].modelos[2]);
    //         break;
    //       case 2:
    //         this.item.push(this.tiendas[_i].modelos[0]);
    //         break;
    //       case 3:
    //         this.item.push(this.tiendas[_i].modelos[1]);
    //         break;
    //       case 4:
    //         this.item.push(this.tiendas[_i].modelos[4]);
    //         break;
    //     }
    //     return this.item;

    //   }
    // }
  }
  getOneFlat(tiendaId: string) {
    this.tiendaRef = this.db.collection('/flat', ref => ref.where('title', '==', tiendaId));
    this.tiendaVar = this.tiendaRef.valueChanges()
    return this.tiendaVar;
  }
  getOneItem(tiendaId: string, item: string) {

    // this.catRef = this.db.collection('flat').doc(tiendaId).collection('modelos');
    // this.catRef.snapshotChanges()
    // this.catVar = this.catRef.valueChanges();
    // console.log('service category');
    // this.catVar.subscribe(res => console.log(res));
    // return this.catVar;

    this.catRef = this.db.collection('/flat').doc(tiendaId).collection('/modelos'), ref => ref.where('name', '==', item);
    this.catVar = this.catRef.valueChanges()
    return this.catVar;
  }

  getCategory(tiendaId: string) {

    // this.catRef = this.db.collection('flat').doc(tiendaId).collection('modelos');
    // this.catRef.snapshotChanges()
    // this.catVar = this.catRef.valueChanges();
    // console.log('service category');
    // this.catVar.subscribe(res => console.log(res));
    // return this.catVar;

    this.catRef = this.db.collection('flat').doc(tiendaId).collection('modelos');
    this.catRef.snapshotChanges()
    this.catVar = this.catRef.valueChanges({ idField: 'id' });
    console.log('service category');
    this.tiendaVar.subscribe(res => console.log(res));
    return this.catVar;



    // this.tiendaRef = this.db.collection('flat').doc(tiendaId).collection('modelos');
    // return this.tiendaRef.snapshotChanges().pipe(
    //   map(actions => {
    //     return actions.map(a => {
    //       const data = a.payload.doc.data();
    //       const id = a.payload.doc.id;
    //       return { id, ...data };
    //     });
    //   })
    // );

    
  }
    



  //   this.todos = this.todosCollection.snapshotChanges().pipe(
  //     map(actions => {
  //       return actions.map(a => {
  //         const data = a.payload.doc.data();
  //         const id = a.payload.doc.id;
  //         return { id, ...data };
  //       });
  //     })
  //   );
  // }
 
  getCat(tiendaId: number) {

    // this.tiendaRef = this.db.collection('flat').doc(tiendaId).collection('modelos');
    // this.tiendaRef.snapshotChanges()
    // console.log('h');
    // this.tiendaVar = this.tiendaRef.valueChanges({ idField: 'id' });
    // console.log(this.tiendaVar);
    // return this.tiendaVar;
 


    //   ...this.tiendas.find(tienda => {
    //     return tiendaId === tienda.id;
    //   })
    //   //Va iterando por cada uno de los valores de tienda
    // };this.db.collection('/expenses', ref => ref.where('expenseId', '==', this.expenseId));

   //return this.db.collection('flat', ref => ref.where('', '==', tiendaId)); 
    //this.docRef = this.db.collection('flat').doc(tiendaId).get();

    // this.docRef = this.db.doc<any>(`flat/${tiendaId}`);
    // console.log(this.docRef.snapshotChanges());
    // console.log('h');
    // this.catVar = this.docRef.valueChanges();
    // console.log(this.catVar);
    // return this.catVar;
  }

  getItem(tiendaId: number, itemId: number) {
    // for (let tienda of this.tiendas) {
    //   if (tienda.id === tiendaId) {
    //     return {
    //       ...tienda.modelos.find(modelos => {
    //         return itemId === modelos.id;
    //       })
    //     }
    //   }
    // }

  }

  deleteItem(tiendaId: number, paramId: number) {

    // for (var _i = 0; _i < this.tiendas.length; _i++) {
    //   if (this.tiendas[_i].id === tiendaId) {
    //     this.tiendas[_i].modelos = this.tiendas[_i].modelos.filter(modelos => {
    //       return paramId !== modelos.id;
    //     })
    //   } else {
    //     this.tiendas = this.tiendas;
    //   }
    //   console.log(this.tiendas);
    // }
  }


  addItem(idCat: number, nameP: string, categoryP: string, imageP: string, priceP: number) {
  //   return new Promise<any>((resolve, reject) => {
  //     // var temp: detailed;

  //     for (var _i = 0; _i < this.tiendas.length; _i++) {
  //       if (this.tiendas[_i].id === idCat) {

  //         // temp = {
  //         //   id: this.tiendas[_i].modelos.length + 1,
  //         //   name: nameP,
  //         //   category: categoryP,
  //         //   image: imageP,
  //         //   price: priceP
  //         // }
  //         // console.log(temp);
  //         // this.tiendas[_i].modelos.push(temp);
  //         console.log('paso');
  //         this.db.collection('/flat').doc('id').collection('/modelos').add(
  //           {
  //             name: nameP,
  //             category: categoryP,
  //             image: imageP,
  //           }).then(res => {
  //             resolve(res);
  //           }, err =>
  //             reject(err))
  //       }
  //     }
  //   });
  // }
  }

}
