import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule  } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { AddPageRoutingModule } from './add-routing.module';

import { AddPage } from './add.page';
import { SharedComponent } from '../../../shared/shared.component';
import { LocationPickerComponent } from 'src/app/shared/pickers/location-picker/location-picker.component';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    IonicModule,
    AddPageRoutingModule,
    SharedComponent,
    LocationPickerComponent
  ],
  declarations: [AddPage]
})
export class AddPageModule {}
