import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { TiendaService } from 'src/app/tienda.service';
import { tienda } from '../../tienda.model';
import { ActivatedRoute, Router } from '@angular/router';;
import { AlertController } from '@ionic/angular';
import { IonicModule } from '@ionic/angular';


@Component({
  selector: 'app-location-picker',
  templateUrl: './add.page.html',
  styleUrls: ['./add.page.scss'],
})
export class AddPage implements OnInit {
  
  add: tienda;
  tiendaId: number;
  form: FormGroup;
  constructor(
    private route: ActivatedRoute,
    private servicio: TiendaService,
    private router: Router,
    private alert: AlertController
  ) { }

  ngOnInit() {
    this.route.paramMap.subscribe(paramMap => {
      this.tiendaId = parseInt(paramMap.get('tiendaId')); 
      this.servicio.getCat(this.tiendaId);
   });
 
 
    this.form = new FormGroup({
      name: new FormControl(null, {
        updateOn: 'blur',
        validators: [Validators.required]
      }),
      category: new FormControl(null, {
        updateOn: 'blur',
        validators: [Validators.required]
      }),
      image: new FormControl(null, {
        updateOn: 'blur',
        validators: [Validators.required]
 }),
      price: new FormControl(null, {
        updateOn: 'blur',
        validators: [Validators.required]
      }),
      location: new FormControl(null, {
        validators: [Validators.required]
      }),
    });
  }

  onClick(){
    this.servicio.addItem(
      this.tiendaId,
      this.form.value.name,
      this.form.value.category,
      "jacket_denim.jpg",
      this.form.value.price
      );
    console.log(this.form);
  }

  OnLocationPickerd(value){
console.log(value);
  }
}
