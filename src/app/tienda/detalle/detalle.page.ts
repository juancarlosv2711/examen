import { Component, OnInit } from '@angular/core';
import { tienda, detailed } from '../tienda.model';
import { ActivatedRoute, Router } from '@angular/router';
import { TiendaService } from 'src/app/tienda.service';
import { AlertController } from '@ionic/angular';
import { Observable } from 'rxjs/Rx';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import { map } from 'rxjs/operators';
import { filter, switchMap } from 'rxjs/operators';
import 'rxjs/add/operator/map'
@Component({
  selector: 'app-detalle',
  templateUrl: './detalle.page.html',
  styleUrls: ['./detalle.page.scss'],
})
export class DetallePage implements OnInit {
  //tiendaActual: tienda;
  modelArray: Observable<tienda[]>;
  flatArray: Observable<tienda[]>;
  catArray: Observable<detailed[]>;
  // variableCat: Array<tienda>;
  variableCat: tienda[];
  //Variable declarada para usar en las funciones


  constructor(
    private route: ActivatedRoute,
    private servicio: TiendaService,
    private router: Router,
    private alert: AlertController
    
    //Aqui en el constructor estamos montando para obtener el Id por el url
  ) { }

  ngOnInit() {
    // this.route.paramMap.subscribe(paramMap => {
    //   if (!paramMap.has('id')) {
    //     console.log('no reconoce el parametro');
    //     return;
    //   } else {
    //     console.log('reconoce el parametro');
    //     const tiendaId = paramMap.get('id');
    //     console.log('El id es =', tiendaId);
    //     this.tiendaActual.forEach
    //     this.tiendaActual = this.servicio.getCategory(tiendaId);
    //     this.tiendaActual.subscribe(variable => this.variableCat = variable);
    //     console.log(this.variableCat);
      
    //   }
    // })


  }
  

  ionViewWillEnter(){

    this.route.paramMap.subscribe(paramMap => {
        if (!paramMap.has('flat')) {
          console.log('no reconoce el parametro');
          return;
        } else {
          console.log('reconoce el parametro');
          const flat = paramMap.get('flat');
          console.log('El id es =', flat);
          this.flatArray = this.servicio.getOneFlat(flat);

          const id = paramMap.get('id');

          this.catArray = this.servicio.getCategory(id);



          // this.modelArray = this.servicio.getCategory(tiendaId)
          // this.flatArray = this.servicio.getOneFlat(tiendaId);
          // this.flatArray.subscribe(res => console.log(res));
          // console.log('prueba=');
          // this.modelArray.subscribe(res => console.log(res));
        }})
      }
    }


    //   if (!paramMap.has('id')) {
    //     console.log('no reconoce el parametro');
    //     return;
    //   } else {
    //     console.log('reconoce el parametro');
    //     const tiendaId = paramMap.get('id');
    //     console.log('El id es =', tiendaId);
    //     this.tiendaActual = this.servicio.getCategory(tiendaId)
    //     this.tiendaActual.subscribe(countries => {
    //       this.variableCat = countries as tienda[]
    //       })
        
    //   }
    // })
    // this.servicio.getCategory().subscribe(res => {
    //   this.variableCat = res;
    // });
   

        // .map((this.tiendaActual) => {
        //   let temp:Array<tienda> = [];
        // if(user){
        //   user.forEach((erg) => {
        //     temp.push(new tienda(erg.id, erg.title, erg.description, erg.modelos, erg.image));
        //   });
        // }
        // return temp;
        // })
        // .subscribe(user => this.variableCat = user);
        
  

  //      
  // ngOnInit() {
  //   this.userService.getUser()
  //     .map((user: Array<any>) => {
  //       let result:Array<User> = [];
  //       if (user) {
  //         user.forEach((erg) => {
  //           result.push(new User(erg.nickname, erg.id, erg.name, erg.nachname, erg.pictURL, erg.city ));
  //         });
  //       }
  //       return result; // <<<=== missing return
  //     })
  //     .subscribe(user => this.user = user);
//   }
  
// })  
// }
// }