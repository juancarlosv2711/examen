import { Component, OnInit } from '@angular/core';
import { tienda, detailed } from '../../tienda.model';
import { ActivatedRoute, Router } from '@angular/router';
import { TiendaService } from 'src/app/tienda.service';
import { AlertController } from '@ionic/angular';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-item',
  templateUrl: './item.page.html',
  styleUrls: ['./item.page.scss'],
})
export class ItemPage implements OnInit {
  catArray: Observable<detailed[]>;


  constructor(
    private route: ActivatedRoute,
    private servicio: TiendaService,
    private router: Router,
    private alert: AlertController
  ) { }

    ngOnInit() {

      this.route.paramMap.subscribe(paramMap => {
        if (!paramMap.has('cat')) {
          console.log('no reconoce el parametro');
          return;
        } else {
          console.log('reconoce el parametro');
          const name = paramMap.get('cat');
          console.log(name);
          const id = paramMap.get('id');
          this.catArray = this.servicio.getOneItem(id, name);
          this.catArray.subscribe(res => console.log(res));
      // this.route.paramMap.subscribe(paramMap => {
      //    this.tiendaId = parseInt(paramMap.get('tiendaId'));
      //   if (!paramMap.has('itemId')){
      //     return;
      //   } else {
      //     const itemId = parseInt(paramMap.get('itemId'));
      //     this.item = this.servicio.getItem(this.tiendaId, itemId);
      //   }
        
      // });
      }
    })
  }

      deleteItem(){

      //    this.alert.create({
      //      header: 'Seguro que desea borrar este Item',
      //      message: 'Realmente deseas borrar este Item',
      //      buttons: [
      //        {
      //          text: 'Cancelar',
      //          role: 'cancel'
      //        },
      //        {
      //          text: 'Borrar',
      //          handler: () => {
      //            this.servicio.deleteItem(this.tiendaId, this.item.id);
      //            this.router.navigate(['./tienda']);
      //          }
      //        }
      //      ] 
      //    })
      //    .then(alertEl => {
      //      alertEl.present();
      //    });
       
     }
    }
  


