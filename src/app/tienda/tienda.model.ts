

export interface tienda{
    id: string;
    title: string;
    description: string;
    modelos: detailed [];
    image: string;
}

export class tienda implements tienda {

  constructor(
    public id: string,
    public title: string,
    public description: string,
    public modelos: detailed [],
    public image: string
  ){}
}

export interface detailed {
  id: string,
  name: string,
  category: string,
  image: string,
  price: number
}