import { Component, OnInit } from '@angular/core';
import { tienda } from './tienda.model';
import { TiendaService } from '../tienda.service';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-tienda',
  templateUrl: './tienda.page.html',
  styleUrls: ['./tienda.page.scss'],
})
export class TiendaPage implements OnInit {

  tiendaVar: Observable<tienda[]>;
  // tiendas: tienda [];----------------------------------------------------------- OLD SPORT ----------------
  

  constructor(private servicio: TiendaService) { }

  ngOnInit() {
    console.log('On Init');
  }

  ionViewWillEnter(){
    
    this.tiendaVar = this.servicio.getAllPhones();
    console.log(this.tiendaVar);
    console.log('will enter again');
  }
  
  ionViewDidEnter(){
    console.log('Did enter');
  }

  ionViewWillLeave(){
    console.log('Will leave');
  }

  ionViewDidLeave(){
    console.log('Did leave');
  }

}
